package controllers;

import java.util.List;

import models.Donation;
import play.mvc.*;

public class Report extends Controller
{
  public static void index()
  {
    List<Donation> donations = Donation.findAll();
    render(donations);
  }
}