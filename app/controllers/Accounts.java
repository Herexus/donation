package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Accounts extends Controller 
{

    public static void index() 
    {
    	List<Candidate> candidates = Candidate.findAll();
        render(candidates);
    }

    public static void signup() 
    {
    	List<Candidate> candidates = Candidate.findAll();
        render(candidates);
    }
//added new strings from user to the logger when a user registers as per story 5
    public static void register (boolean usaCitizen, String firstName, String lastName, String email, String password, String age, String state,
    		String zip, String address1, String address2, String city, String candidate)
    {
        Logger.info(firstName + " " + lastName + " " + email + " " + password + " " + age + " " + state
        		+ " " + zip + " " + address1 + " " + address2 + " " + city);
        
        User user = new User (usaCitizen, firstName, lastName, email, password, age, state, zip, address1, address2, city);
     // Candidate foundCandidate = Candidate.findById(Long.parseLong(candidate));
      // user.candidate = foundCandidate;
        user.save();
        //index();
        login();
    }
    
    public static void edit()
    {
    	index();
    }

    public static void login() 
    {
        render();
    }

    public static void logout() 
    {

        session.clear();
        index();
    }
    
    public static void authenticate(String email, String password) 
    {
       Logger.info("Attempting to authenticate with " + email + ":" + password);

       User user = User.findByEmail(email);
       if ((user != null) && (user.checkPassword(password) == true)) 
       {
           Logger.info("Successfull authentication of  " + user.firstName + " " + user.lastName);
           session.put("logged_in_userid", user.id);
           DonationController.index();
       } 
       else 
       {
           Logger.info("Authentication failed");
           Accounts.login();
       }
     }

    public static User getCurrentUser() 
    {
        String userId = session.get("logged_in_userid");
        if(userId == null)
        {
            return null;
        }
        User logged_in_user = User.findById(Long.parseLong(userId));
        Logger.info("In Accounts controller: Logged in user is " + logged_in_user.firstName);
        return logged_in_user;
    }
}