package controllers;

import models.Donation;
import models.User;
import play.Logger;
import play.mvc.Controller;
import java.util.List;

public class DonationController extends Controller 
{

    public static void index() 
    {
        User user = Accounts.getCurrentUser();
        if (user == null) 
        {
            Accounts.login();
        } 
        else 
        {
            String prog = getPercentTargetAchieved();
            String progress = prog + "%";
            Logger.info("Donation ctrler : user is " + user.email);
            Logger.info("Donation ctrler : percent target achieved " + progress);
            render(user, progress);
        }
    }

    /**
     * Log and save to database amount donated and method of donation, eg.
     * paypal, direct payment
     * 
     * @param amountDonated
     * @param methodDonated
     */
    public static void donate(long amountDonated, String methodDonated, String age) 
    {
        Logger.info("amount donated " + amountDonated + " " + "method donated "
                + methodDonated);

        User user = Accounts.getCurrentUser();
        if (user == null) 
        {
            Logger.info("Donation class : Unable to getCurrentuser");
            Accounts.login();
        } 
        else 
        {
            addDonation(user, amountDonated,methodDonated);
        }
        index();
    }

    /**
     * @param user
     * @param amountDonated
     */
    private static void addDonation(User user, long amountDonated,String methodDonated) 
    {
        Donation bal = new Donation(user, amountDonated, methodDonated);
        bal.save();
    }
    
    private static long getDonationTarget() 
    {
        // TODO Input this value thro' html template admin controlled
        return 20000;
    }

    public static String getPercentTargetAchieved() 
    {
        List<Donation> allDonations = Donation.findAll();
        long total = 0;
        for (Donation donation : allDonations) 
        {
            total += donation.received;
        }
        long target = getDonationTarget();
        long percentachieved = (total * 100 / target);
        String progress = String.valueOf(percentachieved);
        Logger.info("Percent of target achieved (string) " + progress
                + " percentachieved (long)= " + percentachieved);
        return progress;
    }
}