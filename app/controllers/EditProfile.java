package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

import play.db.jpa.Blob;

public class EditProfile extends Controller

{
	
	public static void index()
	  {
	    String userId = session.get("logged_in_userid");
	    User user = User.findById(Long.parseLong(userId));
	    render(user);
	  }
	
	 public static void changeName(String newName)
	    {
	  	    String userId = session.get("logged_in_userid");
	  	    User user = User.findById(Long.parseLong(userId));
	  	    user.firstName = newName;
	  	    user.save();
	  	    Logger.info("First Name changed to " + newName);
	  	    index();
	  	  } 
	    
	    public static void changeSurname(String newSurname)
	    {
	  	    String userId = session.get("logged_in_userid");
	  	    User user = User.findById(Long.parseLong(userId));
	  	    user.lastName = newSurname;
	  	    user.save();
	  	    Logger.info("Last Name changed to " + newSurname);
	  	    index();
	  	  }
	    
	    public static void changeAge(String newAge)
	    {
	  	    String userId = session.get("logged_in_userid");
	  	    User user = User.findById(Long.parseLong(userId));
	  	    user.age = newAge;
	  	    user.save();
	  	    Logger.info("Age changed to " + newAge);
	  	    index();
	  	  } 
	    
	    public static void changeState(String newState)
	    {
	  	    String userId = session.get("logged_in_userid");
	  	    User user = User.findById(Long.parseLong(userId));
	  	    user.state = newState;
	  	    user.save();
	  	    Logger.info("State changed to " + newState);
	  	    index();
	  	  } 
}
