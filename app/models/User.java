 package models;

  import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import java.util.ArrayList;
import java.util.List;

import play.db.jpa.Model;

  @Entity
  public class User extends Model
  {
    public boolean usaCitizen;
    public String firstName;
    public String lastName;


    public String email;
    public String password;
    public String age;
    public String state;
    //added new Strings to the class as per Story 5
    public String zip;
    public String address1;
    public String address2;
    public String city;
    
    @OneToMany(mappedBy = "from", cascade = CascadeType.ALL)
    List<Donation> donations = new ArrayList<Donation>();

    @ManyToOne
    public Candidate candidate;

    public User(boolean usaCitizen,
              String firstName, 
              String lastName, 
              String email, 
              String password,
              String age,
              String state,
              String zip,
    		  String address1,
    		  String address2,
    		  String city
              )
    {
      this.usaCitizen = usaCitizen;
      this.firstName = firstName;
      this.lastName = lastName;
      this.email = email;
      this.password = password;
      this.age = age;
      this.state = state;
      this.zip = zip;
      this.address1 = address1;
      this.address2 = address2;
      this.city = city;
    }

    public static User findByEmail(String email) 
    {
        return find("email", email).first();
    }

    public boolean checkPassword(String password) 
    {
        return this.password.equals(password);
    }

    
    
  }