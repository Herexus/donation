package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import play.db.jpa.Model;

@Entity
public class Candidate extends Model
{
public String candName;
public String candSurname;
public String candAge;
public String candState;

@OneToMany(mappedBy = "from", cascade = CascadeType.ALL)
List<Donation> donations = new ArrayList<Donation>();

@ManyToOne
public Candidate candidate;

@OneToMany(mappedBy = "candidate", cascade = CascadeType.ALL)
List<User> users = new ArrayList<User>();

	  public Candidate(String candName, String candSurname, String candAge, String candState)
	  {
	   this.candName = candName;
	   this.candSurname = candSurname;
	   this.candAge = candAge;
	   this.candState = candState;
	  }

}
