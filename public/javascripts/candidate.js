// Initialization
$('.ui.dropdown')
  .dropdown()
;

// Form validation
$('.ui.form')
.form({

  amountDonated: {
    identifier: 'candName',
    rules: [{
        type: 'empty',
        prompt: 'Please select a candidate to donate to'
      }]
  }
});